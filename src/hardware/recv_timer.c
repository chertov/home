#include "recv_timer.h"
#include "hardware/platform.h"

#define RECIVER_TIMER TIM2

void InitTimeoutTimer(const uint16_t TIM_Prescaler, const uint32_t T_Period) {
    NVIC_InitTypeDef NVIC_InitStructure;
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_TimeBaseStructure.TIM_Prescaler = TIM_Prescaler; // Настраиваем делитель чтобы таймер тикал 1000 раз в секунду

    TIM_TimeBaseStructure.TIM_Period = T_Period*11 - 1;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit( RECIVER_TIMER, &TIM_TimeBaseStructure );

    TIM_ClearITPendingBit( RECIVER_TIMER, TIM_IT_Update );
    TIM_ITConfig( RECIVER_TIMER, TIM_IT_Update, ENABLE );
    TIM_Cmd( RECIVER_TIMER, DISABLE );

    /* Enable the TIM2 gloabal Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
void DeinitTimeoutTimer() {
    TIM_DeInit(RECIVER_TIMER);
}


void EnableTimeoutTimer() { TIM_Cmd( RECIVER_TIMER, ENABLE ); }
void DisableTimeoutTimer() { TIM_Cmd( RECIVER_TIMER, DISABLE ); }

void SetTimeoutTimerDelta(const uint32_t delta) {
    RECIVER_TIMER->CNT = 0;
    RECIVER_TIMER->ARR = delta;
    EnableTimeoutTimer();
}
// запуск таймера таймаута сигнала
void StartTimeoutTimer(const uint32_t delta) {
    RECIVER_TIMER->CNT = 0;
    //RECIVER_TIMER->ARR = delta;
}
// остановка таймера таймаута сигнала
// функция возвращает длину сигнала
uint32_t StopTimeoutTimer() {
    RECIVER_TIMER->SR &= ~TIM_SR_UIF;   // Сбрасываем флаг UIF
    return RECIVER_TIMER->CNT;          // возвращаем длину периода ожидания
}

TimerIRQFunc timeoutTimerInterrupt_func = 0;
void SetTimeoutTimerInterruptFunc(const TimerIRQFunc func) {
    timeoutTimerInterrupt_func = func;
}
// прерывание таймера таймаута сигнала
void TIM2_IRQHandler(void) {
    if(timeoutTimerInterrupt_func != 0) (*timeoutTimerInterrupt_func)();
}
