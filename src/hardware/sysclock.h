#pragma once
#include <stdint.h>

void InitSysClockTimer(const uint16_t TIM_Prescaler);

// Получить текущее значение счетчика системного таймера
uint32_t GetSysClockTicks();

// Получить разность между last_ticks и текущим значеним счетчика системного таймера.
// Текущее значение счетчика системного таймера будет сохранено в last_ticks
uint32_t GetSysClockDelta(uint32_t *last_ticks);