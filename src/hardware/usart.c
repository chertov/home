#include "usart.h"
#include "hardware/platform.h"
#include "logic/uart_recv.h"

void USART1_Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef  NVIC_InitStructure;

    // Включаем тактирование порта C
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
    // Включаем тактирование USART1
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

    // Активируем альтернативные функции пинов PC4-USART1_TX и PC5-USART1_RX
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource4, GPIO_AF_7);   // USART1_TX PC4
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource5, GPIO_AF_7);   // USART1_RX PC5

    // Инициализируем порт C
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    /* USART configuration */
    USART_InitStructure.USART_BaudRate            = 115200;
    USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits            = USART_StopBits_1;
    USART_InitStructure.USART_Parity              = USART_Parity_No ;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART1, &USART_InitStructure);
    USART_Cmd(USART1, ENABLE);


    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);


    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void USART1_IRQHandler (void)
{
    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
        recive_callback( USART_ReceiveData(USART1) );
}

void USART2_Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    
    // Включаем тактирование порта A
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    // Включаем тактирование USART2
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    
    // Активируем альтернативные функции пинов PA2-USART1_TX и PA3-USART1_RX
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_7);    // USART2_TX PA2
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_7);    // USART2_RX PA3

    // Инициализируем порт A
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    /* USART configuration */
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART2, &USART_InitStructure);
    USART_Cmd(USART2, ENABLE);
}

void Init_usart(void)
{
    USART1_Init();
    USART2_Init();
}

void DeInit_usart(void)
{
    USART_DeInit(USART1);
    USART_DeInit(USART2);
}

void send_to_uart(uint8_t data) {
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    USART_SendData(USART1, data);
}

void send8(uint8_t data) {
    while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
    USART_SendData(USART2, data);
}
void send16(uint16_t data) {
		send8(((uint8_t*)(&data))[0]);
		send8(((uint8_t*)(&data))[1]);
}
void send32(uint32_t data) {
		send16(((uint16_t*)(&data))[0]);
		send16(((uint16_t*)(&data))[1]);
}
