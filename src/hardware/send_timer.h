#pragma once
#include <stdint.h>


typedef void(*TimerIRQFunc)(void);

void InitSendTimer(const uint16_t TIM_Prescaler);
void DeinitSendTimer();

void SetSendTimerInterruptFunc(const TimerIRQFunc interrupt_func);

void EnableSendTimer();
void DisableSendTimer();

void SetSendTimerDelta(const uint32_t delta);
uint32_t StopSendTimer();