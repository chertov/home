#include "leds.h"

#include "hardware/platform.h"

#define LED_GREEN_PORT  GPIOE
#define LED_GREEN_PIN   GPIO_Pin_11

#define LED_ORANGE_PORT  GPIOE
#define LED_ORANGE_PIN   GPIO_Pin_10

#define LED_RED_PORT    GPIOE
#define LED_RED_PIN     GPIO_Pin_9

#define LED_BLUE_PORT    GPIOE
#define LED_BLUE_PIN     GPIO_Pin_8

#define PINS LED_BLUE_PIN | LED_RED_PIN | LED_ORANGE_PIN | LED_GREEN_PIN

void Init_leds(void)
{
    // Включаем тактирование порта E
    GPIO_InitTypeDef PORTE_init_struct;
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE, ENABLE);
    PORTE_init_struct.GPIO_Mode = GPIO_Mode_OUT;
    PORTE_init_struct.GPIO_Pin = PINS;
    PORTE_init_struct.GPIO_OType = GPIO_OType_PP;
    PORTE_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &PORTE_init_struct);
}
void DeInit_leds(void)
{
    GPIO_DeInit(GPIOE);
}

void toggle_led(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{ 
    if (GPIO_ReadOutputDataBit(GPIOx, GPIO_Pin))
        GPIO_ResetBits(GPIOx, GPIO_Pin);
    else
        GPIO_SetBits(GPIOx, GPIO_Pin);
}

void led_green_on(void)         { GPIO_SetBits  ( LED_GREEN_PORT, LED_GREEN_PIN ); }
void led_green_off(void)        { GPIO_ResetBits( LED_GREEN_PORT, LED_GREEN_PIN ); }
void led_green_toggle(void)     { toggle_led    ( LED_GREEN_PORT, LED_GREEN_PIN ); }

void led_orange_on(void)        { GPIO_SetBits  ( LED_ORANGE_PORT, LED_ORANGE_PIN); }
void led_orange_off(void)       { GPIO_ResetBits( LED_ORANGE_PORT, LED_ORANGE_PIN); }
void led_orange_toggle(void)    { toggle_led    ( LED_ORANGE_PORT, LED_ORANGE_PIN); }

void led_red_on(void)           { GPIO_SetBits  ( LED_RED_PORT, LED_RED_PIN); }
void led_red_off(void)          { GPIO_ResetBits( LED_RED_PORT, LED_RED_PIN); }
void led_red_toggle(void)       { toggle_led    ( LED_RED_PORT, LED_RED_PIN); }

void led_blue_on(void)          { GPIO_SetBits  ( LED_BLUE_PORT, LED_BLUE_PIN); }
void led_blue_off(void)         { GPIO_ResetBits( LED_BLUE_PORT, LED_BLUE_PIN); }
void led_blue_toggle(void)      { toggle_led    ( LED_BLUE_PORT, LED_BLUE_PIN); }
