#pragma once
#include <stdint.h>

typedef void(*TimerIRQFunc)(void);

void InitTimeoutTimer(const uint16_t TIM_Prescaler, const uint32_t T_Period);
void DeinitTimeoutTimer();

void SetTimeoutTimerInterruptFunc(const TimerIRQFunc interrupt_func);

void EnableTimeoutTimer();
void DisableTimeoutTimer();

void SetTimeoutTimerDelta(const uint32_t delta);

// запуск таймера таймаута сигнала
void StartTimeoutTimer(const uint32_t delta);

// остановка таймера таймаута сигнала
// функция возвращает длину сигнала
uint32_t StopTimeoutTimer();