#pragma once
#include <stdint.h>

void Init_usart(void);
void DeInit_usart(void);

void send_to_uart(uint8_t data);
void send8(uint8_t data);
void send16(uint16_t data);
void send32(uint32_t data);

