#pragma once
#include "hardware/platform.h"

void Init_adc(void);
void DeInit_adc(void);

uint16_t get_adc_value(void);
