#pragma once

#define     LED_PORT    GPIOC
#define     LED_BLUE    GPIO_Pin_8
#define     LED_GREEN   GPIO_Pin_9

void Init_leds(void);
void DeInit_leds(void);

void led_green_on(void);
void led_green_off(void);
void led_green_toggle(void);

void led_orange_on(void);
void led_orange_off(void);
void led_orange_toggle(void);

void led_red_on(void);
void led_red_off(void);
void led_red_toggle(void);

void led_blue_on(void);
void led_blue_off(void);
void led_blue_toggle(void);
