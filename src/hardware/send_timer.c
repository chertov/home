#include "send_timer.h"
#include "hardware/platform.h"

#define     SENDER_TIMER    TIM3

void InitSendTimer(const uint16_t TIM_Prescaler) {
    NVIC_InitTypeDef NVIC_InitStructure;

    //// RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    //#ifdef ENABLE_LONGTIME
    //    TIM4->PSC = LONGTIME;           // Настраиваем делитель что таймер тикал 1000 раз в секунду
    //#else
    //    TIM4->PSC = 24 - 1;             // Настраиваем делитель что таймер тикал 1000 раз в секунду
    //#endif

    //TIM4->ARR = 1;                  // Чтоб прерывание случалось раз в секунду
    //TIM4->DIER |= TIM_DIER_UIE;     // Разрешаем прерывание от таймера
    //TIM4->CR1 |= TIM_CR1_CEN;       // Начать отсчёт!
    //NVIC_EnableIRQ(TIM4_IRQn);      // Разрешение TIM6_DAC_IRQn прерывания


    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_TimeBaseStructure.TIM_Prescaler = TIM_Prescaler;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    TIM_TimeBaseStructure.TIM_Period = 1;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit( SENDER_TIMER, &TIM_TimeBaseStructure );

    TIM_ClearITPendingBit( SENDER_TIMER, TIM_IT_Update );
    TIM_ITConfig( SENDER_TIMER, TIM_IT_Update, ENABLE );
    TIM_Cmd( SENDER_TIMER, DISABLE );

    /* Enable the TIM4 gloabal Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    //TIM4_IRQHandler = SENDER_TIMER_IRQHandler;
}
void DeinitSendTimer() {
    TIM_DeInit( SENDER_TIMER );
}


void EnableSendTimer() { TIM_Cmd( SENDER_TIMER, ENABLE ); }
void DisableSendTimer() { TIM_Cmd( SENDER_TIMER, DISABLE ); }

void SetSendTimerDelta(const uint32_t delta) {
    SENDER_TIMER->CNT = 0;
    SENDER_TIMER->ARR = delta;
}
uint32_t StopSendTimer() {
    SENDER_TIMER->SR &= ~TIM_SR_UIF;    // Сбрасываем флаг UIF
    return SENDER_TIMER->CNT;
}
TimerIRQFunc SendTimerInterruptFunc = 0;
void SetSendTimerInterruptFunc(const TimerIRQFunc interrupt_func) {
    SendTimerInterruptFunc = interrupt_func;
}

void TIM3_IRQHandler(void) {
    if(SendTimerInterruptFunc != 0) (*SendTimerInterruptFunc)();
}
