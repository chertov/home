#include "recv_timer.h"
#include "hardware/platform.h"

#define SYSTICK_RANGE 0x1000000 // 24 bit

void InitSysClockTimer(const uint16_t TIM_Prescaler) {
    NVIC_InitTypeDef NVIC_InitStructure;
    /*Вызов функции конфигурации системного таймера SysTick.
    Эта функция находится в файле core_cm3.h и:
    --Задает источник тактирования системного таймера (по умолчанию это SYSCLK = 24 МГц, 
    другой возможный вариант  - SysTick тактируется от SYSCLK/8);
    --Задает уровень приоритета прерывания;
    --Сбрасывает флаг ожидания прерывания, если он выставлен;
    --Заносит в нужный регистр перезагружаемое значение для декрементирующего счетчика,
        которое вычисляется по формуле:
                Reload Value = SysTick Counter Clock (Hz) x  Desired Time base (s),
                для базовой задержки длительностью 1 мс получим величину
                Reload Value = 24 000 000 Гц х 0,001 с = 24 000 
        (Необходимо самостоятельно посчитать эту величину и вставить в качестве
        параметра при вызове функции);
    --Обнуляет счетчик
    --Запускает счет системного таймера*/
    //SysTick_Config(24000);
    
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
    SysTick_Config(SYSTICK_RANGE-1);
//    /* Super priority for SysTick - is done over absolutely everything. */
//    NVIC_InitStructure.NVIC_IRQChannel = SysTick_IRQn;
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//    NVIC_Init(&NVIC_InitStructure);
//    NVIC_SetPriority(SysTick_IRQn, 0);
}

volatile long long SysTickMajor = SYSTICK_RANGE;
             
uint32_t GetSysClockTicks() {
    //return SysTickMajor;
    return SysTick->VAL;    // возвращаем длину периода ожидания
}
uint32_t GetSysClockDelta(uint32_t *last_ticks) { 
    static uint32_t tmp, val;
    val = SysTick->VAL;
    tmp = (*last_ticks - val) % SYSTICK_RANGE;
    (*last_ticks) = val;
    return tmp;    // возвращаем длину периода ожидания
}


//char str[128];
void SysTick_Handler(void) {
//    SysTickMajor+=SYSTICK_RANGE;

//    _int2str(str, GetSysClockTicks(), 10, 1, 0);
//    Log_nn("SysTick_Handler: ");
//    Log_nn(str);
//    Log_nn("\n");
//    _int2str(str, SYSTICK_RANGE, 10, 1, 0);
//    Log_nn("SysTick_Handler: ");
//    Log_nn(str);
//    Log_nn("\n");
}
