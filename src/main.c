#include "tools/log.h"
#include "tools/logger.h"
#include "io/data.h"
#include "io/receiver.h"
#include "io/sender.h"
#include "hardware/leds.h" 
#include "hardware/platform.h"
#include "tools/tools.h"

#include "tools/random.h"

#include "bootloader.h"

void test_frame(Frame *frame)
{
    u8 test = 0;

    frame->frame.prio = 21845;
    frame->frame.src = 7;
    frame->frame.dst = 333;

    frame->frame.size = 0;
    frame->data[frame->frame.size++] = 'H';
    frame->data[frame->frame.size++] = 'e';
    frame->data[frame->frame.size++] = 'l';
    frame->data[frame->frame.size++] = 'l';
    frame->data[frame->frame.size++] = 'o';
    frame->data[frame->frame.size++] = ',';
    frame->data[frame->frame.size++] = ' ';
    frame->data[frame->frame.size++] = 'W';
    frame->data[frame->frame.size++] = 'o';
    frame->data[frame->frame.size++] = 'r';
    frame->data[frame->frame.size++] = 'l';
    frame->data[frame->frame.size++] = 'd';
    frame->data[frame->frame.size++] = '!';
    frame->data[frame->frame.size++] = '\0';
    test = test_crc_frame(frame);
    crc_frame(frame);
    test = test_crc_frame(frame);
    crc_frame(frame);
}

u32 counter = 0;

void fill_frame(Frame *frame)
{
    char str[16];
    u8 test = 0;

    frame->frame.prio = 21845;
    frame->frame.src = 7;
    frame->frame.dst = 333;

    frame->frame.size = 0;
    frame->data[frame->frame.size++] = 'H';
    frame->data[frame->frame.size++] = 'e';
    frame->data[frame->frame.size++] = 'l';
    frame->data[frame->frame.size++] = 'l';
    frame->data[frame->frame.size++] = 'o';
    frame->data[frame->frame.size++] = ',';
    frame->data[frame->frame.size++] = ' ';
    frame->data[frame->frame.size++] = 'W';
    frame->data[frame->frame.size++] = 'o';
    frame->data[frame->frame.size++] = 'r';
    frame->data[frame->frame.size++] = 'l';
    frame->data[frame->frame.size++] = 'd';
    frame->data[frame->frame.size++] = '!';
    frame->data[frame->frame.size++] = ' ';
    frame->data[frame->frame.size++] = ' ';

    _int2str(str, counter++, 10, 1, 0);

    frame->data[frame->frame.size++] = str[0];
    frame->data[frame->frame.size++] = str[1];
    frame->data[frame->frame.size++] = str[2];
    frame->data[frame->frame.size++] = str[3];
    frame->data[frame->frame.size++] = str[4];
    frame->data[frame->frame.size++] = str[5];
    frame->data[frame->frame.size++] = str[6];
    frame->data[frame->frame.size++] = str[7];
    frame->data[frame->frame.size++] = str[8];
    frame->data[frame->frame.size++] = str[9];
    frame->data[frame->frame.size++] = str[10];
    frame->data[frame->frame.size++] = str[11];
    frame->data[frame->frame.size++] = str[12];
    frame->data[frame->frame.size++] = str[13];
    frame->data[frame->frame.size++] = str[14];
    frame->data[frame->frame.size++] = str[15];

    frame->data[frame->frame.size++] = '\0';
    crc_frame(frame);
}

Frame get_frame(u32 counter)
{
    Frame frame;
    char str[16];
    u8 test = 0;
    
    frame.frame.prio = 21845;
    frame.frame.src = 7;
    frame.frame.dst = 333;

    frame.frame.size = 0;
    frame.frame.data[frame.frame.size++] = 'H';
    frame.frame.data[frame.frame.size++] = 'e';
    frame.frame.data[frame.frame.size++] = 'l';
    frame.frame.data[frame.frame.size++] = 'l';
    frame.frame.data[frame.frame.size++] = 'o';
    frame.frame.data[frame.frame.size++] = ',';
    frame.frame.data[frame.frame.size++] = ' ';
    frame.frame.data[frame.frame.size++] = 'W';
    frame.frame.data[frame.frame.size++] = 'o';
    frame.frame.data[frame.frame.size++] = 'r';
    frame.frame.data[frame.frame.size++] = 'l';
    frame.frame.data[frame.frame.size++] = 'd';
    frame.frame.data[frame.frame.size++] = '!';
    frame.frame.data[frame.frame.size++] = ' ';
    frame.frame.data[frame.frame.size++] = ' ';

    _int2str(str, counter, 10, 1, 0);

    frame.frame.data[frame.frame.size++] = str[0];
    frame.frame.data[frame.frame.size++] = str[1];
    frame.frame.data[frame.frame.size++] = str[2];
    frame.frame.data[frame.frame.size++] = str[3];
    frame.frame.data[frame.frame.size++] = str[4];
    frame.frame.data[frame.frame.size++] = str[5];
    frame.frame.data[frame.frame.size++] = str[6];
    frame.frame.data[frame.frame.size++] = str[7];
    frame.frame.data[frame.frame.size++] = str[8];
    frame.frame.data[frame.frame.size++] = str[9];
    frame.frame.data[frame.frame.size++] = str[10];
    frame.frame.data[frame.frame.size++] = str[11];
    frame.frame.data[frame.frame.size++] = str[12];
    frame.frame.data[frame.frame.size++] = str[13];
    frame.frame.data[frame.frame.size++] = str[14];
    frame.frame.data[frame.frame.size++] = str[15];

    frame.data[frame.frame.size++] = '\0';
    crc_frame(&frame);
    return frame;
}

void Init_rcc()
{
    ErrorStatus HSEStartUpStatus;
    RCC_ClocksTypeDef RCC_Clocks;

    RCC_DeInit();
    RCC_HSEConfig(RCC_HSE_ON);
    HSEStartUpStatus = RCC_WaitForHSEStartUp();
    
    if(HSEStartUpStatus == SUCCESS)
    {
        // HCLK = SYSCLK
        RCC_HCLKConfig(RCC_SYSCLK_Div1);
        // PCLK2 = HCLK
        RCC_PCLK2Config(RCC_HCLK_Div1);
        // PCLK1 = HCLK/2
        RCC_PCLK1Config(RCC_HCLK_Div2);

        // PLLCLK = 8 MHz * 9 = 72 MHz
        RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_9);
        
        // Разрешение PLL
        RCC_PLLCmd(ENABLE);
    
        // Ожидание готовности PLL
        while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET) {};
        
        // выбор PLL как источника системного тактирования
        RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
        
        // Ожидание готовности PLL для использования как источника системного тактирования
        while(RCC_GetSYSCLKSource() != 0x08) {}

        //RCC_GetClocksFreq(&RCC_Clocks);
        //(void) SysTick_Config(RCC_Clocks.HCLK_Frequency / SHORTTIME);
        while(SysTick_Config(SystemCoreClock / 1000));
    }
}
              
#define SYSTICK_RANGE 0x1000000 // 24 bit
void SenderLoop() {
    char str[128];
    Frame frame;
    u32 counter = 0;   
    u32 last_tick = 0;

    Log_nn("Start send loop");
    Init_Sender();  
    while(1)
    {
        led_red_on();
        Delay_ms(300);		// Просто чтобы светодиод светился

        _int2str(str, counter++, 10, 1, 0);
        Log_nn("Send: ");
        Log_nn(str);
        Log_nn("\n");

        fill_frame(&frame);
        SendFrame(&frame);
        led_red_off();
        Delay_ms(1000);
    }
}

int main(void)
{
    Frame frame;

    u32 uuid = 0;
    u32 soft_uuid = 0;
    u32 counter = 0;
    
    uint16_t uart_data;

    SystemInit();
    __disable_irq();
    // инициализация таблицы векторов прерываний после прыжка
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, AppilcationAddress);
    __enable_irq();
    
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE); 		// Включаем модуль подсчета CRC

    Init_log();
    //Init_logger();
    __enable_irq();
    InitSysClockTimer();

    Init_leds();

    Init_data_pin();
    Init_Reciever();

    Log("Start home");
    //led_green_on();
    //NVIC_SystemReset();

    Init_random();
    Log("init random");

    uuid = get_uuid();
    Log_nn("My uuid: ");
    Log_uint_nn(uuid);
    Log_nn("\n");
    Log("Test");

    SenderLoop();

    if(uuid == 1730617847)
    //if(uuid == 1513901522)
    {
        Log_nn("Sender");
        Init_Sender();
        while(1)
        {
            led_red_on();
            Delay(100000);

    //        _int2str(str, counter++, 10, 1, 0);
    //        Log_nn("Tick: ");
    //        Log_nn(str);
    //        Log_nn("\n");

            frame = get_frame(counter);
            counter++;
    //        Log_nn("Frame: ");
    //        Log_nn_len((const unsigned char*)(frame.frame.data), frame.frame.size);
    //        Log("");
            SendFrame(&frame);
            led_red_off();
            Delay(100000);
        }
    }

//    if(uuid == 1185680314)
//    {
//        Log_nn("Reciever");
//        while(1)
//        {
//            Delay(1000000);
//            //Log_uint(random());
//        }
//    }

    while(1)
    {
//        if (USART_GetITStatus(USART1, USART_IT_RXNE) ) {
//            USART_ClearITPendingBit(USART1, USART_IT_RXNE);
//            USART_ClearFlag(USART1, USART_FLAG_RXNE);
//            uart_data = USART_ReceiveData(USART1);
//            Log_nn((char*)&uart_data);
//        }
        if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
        {
            uart_data = USART_ReceiveData(USART1);
            send_to_uart(uart_data);
        }
    }
}
