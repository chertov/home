#include "receiver.h"

#include "hardware/platform.h"
#include "config.h"
#include "tools/tools.h"
#include "tools/logger.h"
#include "hardware/leds.h"

#ifdef USE_TIMEOUT_TIMER
    #include "hardware/recv_timer.h"
#else
    #include "hardware/sysclock.h"
    uint32_t last_ticks;
#endif

void INT_Up();
void INT_Down();
void INT_Timeout();

// инициализация приемника
void Init_Reciever() { 
    Init_Frames();
#ifdef USE_TIMEOUT_TIMER
    #ifdef ENABLE_LONGTIME
        InitTimeoutTimer(LONGTIME, T);         // Настраиваем делитель что таймер тикал 1000 раз в секунду
    #else
        InitTimeoutTimer(SHORTTIME, T);        // Настраиваем делитель что таймер тикал 1000 раз в секунду
    #endif
#endif
}

void DeInit_Reciever() {
#ifdef USE_TIMEOUT_TIMER
    DeinitTimeoutTimer();
#endif
}

#ifdef ENABLE_LONGTIME
char str[256];
#endif
char str[256];


void INT_Up(void) {
    // запукаем таймер ожидания сигнала СТАРТ 
#ifdef USE_TIMEOUT_TIMER
    StartTimeoutTimer(T_SIZE_TIMEOUT);
    EnableTimeoutTimer();
#else
    GetSysClockDelta(&last_ticks);
#endif

#ifdef DEBUG
    led_blue_on();
    #ifdef ENABLE_LONGTIME
        //Log("Up");
    #endif
#endif
}

void INT_Down(void) {
    static uint32_t frame_time = 0;
    // останавливаем таймер таймаута, получаем время от Up до Down
#ifdef USE_TIMEOUT_TIMER
    frame_time = StopTimeoutTimer();
    DisableTimeoutTimer(); 
#else
    frame_time = GetSysClockDelta(&last_ticks) / 24;
#endif
//    _int2str(str, frame_time, 10, 1, 0);
//    Log_nn("frame_time: ");
//    Log_nn(str);
//    _int2str(str, frame_time_alt, 10, 1, 0);
//    Log_nn("    frame_time_alt: ");
//    Log_nn(str);
//    Log_nn("\n");
    
    // количество интервалов равно
    if(frameIs(frame_time, T_SIZE_INIT))
        receive_init();                 // сигналу СТАРТ
    else if (frameIs(frame_time, T_SIZE_ONE))
        receive_one();                  // единице данных
    else if(frameIs(frame_time, T_SIZE_ZERO))
        receive_zero();                 // нулю данных
    else receive_unknow(frame_time);       // получили фрагмент неизвестного типа

#ifdef DEBUG
    led_blue_off();
    #ifdef ENABLE_LONGTIME
//        _int2str(str, t_count, 10, 1, 0);
//        Log_nn("Down   ");
//        Log(str);
    #endif
#endif
}

// обработка таймаута сигнала
void INT_Timeout(void) {
    // считаем таймауты для статистики
    timer_timeouts++;
    receive_timeout();
#ifdef DEBUG
    #ifdef ENABLE_LONGTIME
        Log("Timeout");
    #endif
#endif
}

//// Обработчик прерывания EXTI0
//void EXTI0_IRQHandler(void)
//{
//    if (EXTI_GetITStatus(EXTI_Line0) != RESET)
//    {
//        // определяем фронт или спад на входе датапина
//        if(GPIO_ReadInputDataBit(DATA_PORT, DATA_PIN))
//            INT_Down();
//        else
//            INT_Up();

//        EXTI_ClearITPendingBit(EXTI_Line0);
//    }

//    EXTI->PR |= 0x01;                // Очищаем флаг
//}
