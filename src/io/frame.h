#pragma once
#include "hardware/platform.h"
#include "config.h"

typedef struct
{
    u16 prio;
    u16 src;
    u16 dst;
    u16 size;
    u32 data[FRAME_MAX_DATA_SIZE];
    u32 crc;
} FrameData;

typedef union
{
    u32 data[FRAME_MAX_DATA_SIZE + 3];
    FrameData frame;
} Frame;

void sendFrameToUart(Frame *frame);
