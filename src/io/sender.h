#pragma once

#include "hardware/platform.h"
#include "config.h"
#include "frame.h"

void Init_Sender(void);
void DeInit_Sender(void);

void SendFrame(Frame *frame);