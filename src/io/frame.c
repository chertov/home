#include "frame.h"

#include "config.h"
#include "tools/logger.h"
#include "state.h"
#include "hardware/usart.h"

static u8 state = LISTEN;

Frame receive_frame[2];
void Init_Frames()
{
    //receive_frame = (Frame *)malloc( 1*sizeof(Frame) );

    if(receive_frame == 0)
    {
        #ifdef DEBUG
            Log("Fail");
        #endif
    }
    else
    {
        #ifdef DEBUG
            Log("Success");
        #endif
    }
}

static u16 receive_bits_count = 0;
static u16 bit_pos = 0;
static u16 int_pos = 0;


void StartOfFrame()
{

}



void EndOfFrame()
{
    static char str[50];
    u8 crct;
    
    static unsigned int rcv_correct = 0;
    static unsigned int rcv_incorrect = 0;
    
    unsigned int i;
    state = LISTEN;
    receive_bits_count = 0;
    bit_pos = 0;
    int_pos = 0;

    //Log("EndOfFrame");
    crct = test_crc_frame(receive_frame);

    //Log_nn(receive_frame[0].data[5]);

    if(crct)
    {
        Log_nn("Correct  ");
        rcv_correct++;
    }
    else
    {
        Log_nn("Incorrect");
        rcv_incorrect++;
    }

    _int2str(str, rcv_correct, 10, 1, 0);
    Log_nn("        correct: ");
    Log_nn(str);

    _int2str(str, rcv_incorrect, 10, 1, 0);
    Log_nn("   incorrect: ");
    Log_nn(str);

    _int2str(str, receive_frame[0].frame.crc, 10, 1, 0);
    Log_nn("   crc: ");
    Log_nn(str);

    Log_nn("\n");

    for(i = 0; i < 30; i++) {
        str[i] = receive_frame[0].frame.data[i];
        //send_to_uart2(str[i]);
    }
    
    sendFrameToUart(&(receive_frame[0]));

    Log_nn(str);
    Log_nn("\n");
}

void sendFrameToUart(Frame *frame) {
    u16 i = 0;
    if (frame == 0) return;

    send16(frame->frame.prio);
    send16(frame->frame.src);
    send16(frame->frame.dst);
    send16(frame->frame.size);
    for(i = 0; i < frame->frame.size; i++) send32(frame->frame.data[i]);
        send32(frame->frame.crc);
}

void ErrorFrame()
{
    state = LISTEN;
    receive_bits_count = 0;
    bit_pos = 0;
    int_pos = 0;

}

void set_data(u8 bit)
{
    if(state == RECEIVE_DATA)
    {
        int_pos = receive_bits_count / 32;
        bit_pos = receive_bits_count - int_pos*32;

#ifdef DEBUG
    #ifdef ENABLE_LONGTIME
//    static char str[16];
//    _int2str(str, bit, 10, 1, 0);
//    Log_nn("recv bit: ");
//    Log_nn(str);
//    _int2str(str, receive_bits_count, 10, 1, 0);
//    Log_nn("      rbc: ");
//    Log_nn(str);
//    _int2str(str, bit_pos, 10, 1, 0);
//    Log_nn("      bit_pos: ");
//    Log_nn(str);
//    _int2str(str, int_pos, 10, 1, 0);
//    Log_nn("      int_pos: ");
//    Log_nn(str);
//    Log_nn("\n");
    #endif
#endif

        if( int_pos > 1 )
        {
            if(int_pos == receive_frame->frame.size + 2)
            {
                if(bit) receive_frame->frame.crc |= 1 << bit_pos;
                else receive_frame->frame.crc &= ~(1 << bit_pos);

                if( bit_pos == 31 )
                {
                    EndOfFrame();
                    return;
                }
            }
            else
            {
                if(bit) receive_frame->frame.data[int_pos - 2] |= 1 << bit_pos;
                else receive_frame->frame.data[int_pos - 2] &= ~(1 << bit_pos);
            }
        }
        else
        {
            if(bit) receive_frame->data[int_pos] |= 1 << bit_pos;
            else receive_frame->data[int_pos] &= ~(1 << bit_pos);
        }
    }

    receive_bits_count++;
}

void receive_one()
{
    rcv_good++;
    set_data(1);
}

void receive_zero()
{
    rcv_good++;
    set_data(0);
}

void receive_init()
{
    receive_bits_count = 0;
    bit_pos = 0;
    int_pos = 0;

    rcv_init++;
//  Log_nn("\n");
//  Log_nn("\n");
//  Log("Init");
    state = RECEIVE_DATA;
}

void receive_unknow(u8 t_count)
{
    static char str[16];
    
    rcv_unknow++;

    _int2str(str, t_count, 10, 1, 0);
    Log_nn("unknow: ");
    Log_nn(str);
    Log_nn("\n");

    ErrorFrame();
}

void receive_timeout()
{
    rcv_timeout++;
    Log("timeout");
    ErrorFrame();
}
