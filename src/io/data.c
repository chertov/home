#include "data.h"

#include "tools/log.h"
#include "frame.h"
#include "tools/tools.h"
#include "sender.h"
#include "receiver.h"

#define DATA_PORT   GPIOB
#define DATA_PIN    GPIO_Pin_1


Frame data_frame[1];

void test_data_frame(Frame *frame)
{
    u8 test = 0;
    
    frame->frame.prio = 21845;
    frame->frame.src = 7;
    frame->frame.dst = 333;

    frame->frame.size = 0;
    frame->data[frame->frame.size++] = 'H';
    frame->data[frame->frame.size++] = 'e';
    frame->data[frame->frame.size++] = 'l';
    frame->data[frame->frame.size++] = 'l';
    frame->data[frame->frame.size++] = 'o';
    frame->data[frame->frame.size++] = ',';
    frame->data[frame->frame.size++] = ' ';
    frame->data[frame->frame.size++] = 'W';
    frame->data[frame->frame.size++] = 'o';
    frame->data[frame->frame.size++] = 'r';
    frame->data[frame->frame.size++] = 'l';
    frame->data[frame->frame.size++] = 'd';
    frame->data[frame->frame.size++] = '!';
    frame->data[frame->frame.size++] = '\0';
    crc_frame(frame);
}

void Button_Init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef  NVIC_InitStructure;
    
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    
    EXTI_InitStructure.EXTI_Line = EXTI_Line0;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
 
 
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}


void Init_data_pin(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    EXTI_InitTypeDef  EXTI_InitStructure;
    NVIC_InitTypeDef  NVIC_InitStructure;

    test_data_frame(data_frame);
    Button_Init();

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
    
    GPIO_DeInit(DATA_PORT);
    GPIO_InitStructure.GPIO_Pin   = DATA_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_Init(DATA_PORT, &GPIO_InitStructure);

    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource1);

    EXTI_InitStructure.EXTI_Line = EXTI_Line1;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void DeInit_data_pin(void)
{
    GPIO_DeInit(DATA_PORT);
}

void data_pin_setbit(u8 bit)
{
    GPIO_WriteBit(DATA_PORT, DATA_PIN, bit);
}

//// Обработчик прерывания EXTI0
//void EXTI0_IRQHandler(void)
//{
//  if (EXTI_GetITStatus(EXTI_Line0) != RESET)
//  {
//      // определяем фронт или спад на входе датапина
//      if(GPIO_ReadInputDataBit(DATA_PORT, DATA_PIN))
//          INT_Down();
//      else
//          INT_Up();

//      EXTI_ClearITPendingBit(EXTI_Line0);
//  }

//  EXTI->PR |= 0x01;           // Очищаем флаг
//    //EXTI_ClearFlag(EXTI_Line0);
//}


    
// Обработчик прерывания EXTI0
void EXTI0_IRQHandler(void)
{    
    EXTI_ClearFlag(EXTI_Line0);
    if (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == 1)
    {
        SendFrame(data_frame);
    }
    else
    {    
        //Log("Up PA0");
    }
}

//// Обработчик прерывания EXTI0
//void EXTI1_IRQHandler(void)
//{    
//    EXTI_ClearFlag(EXTI_Line1);
////    if (GPIO_ReadInputDataBit(DATA_PORT, DATA_PIN) == 1)
////        Log("Data pin down");
////    else
////        Log("Data pin up");
//}

// Обработчик прерывания EXTI0
void EXTI1_IRQHandler(void)
{
    if (EXTI_GetITStatus(EXTI_Line1) != RESET)
    {
        // определяем фронт или спад на входе датапина
        if(GPIO_ReadInputDataBit(DATA_PORT, DATA_PIN))
            INT_Down();
        else
            INT_Up();

        EXTI_ClearITPendingBit(EXTI_Line1);
    }

    EXTI_ClearFlag(EXTI_Line1);
    //EXTI->PR |= 0x01;         // Очищаем флаг
}