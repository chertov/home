#pragma once

typedef enum {
    LISTEN = 0,

    RECEIVE_INIT = 1,
    RECEIVE_DST = 2,
    RECEIVE_SRC = 3,
    RECEIVE_DATA_LEN = 4,
    RECEIVE_DATA = 5,
    RECEIVE_CRC = 6,

    SEND_INIT = 7,
    SEND_DST = 8,
    SEND_SRC = 9,
    SEND_DATA_LEN = 10,
    SEND_DATA = 11,
    SEND_CRC = 12

} RECEIVE_STATE;
