#include "sender.h"

#include "data.h"

#include "config.h"
#include "state.h"

#include "hardware/send_timer.h"

//int data[] = {0,0,1,0,1,0,1};


void SendTIMInterrup(void);

void Init_Sender() {
#ifdef ENABLE_LONGTIME
    InitSendTimer(LONGTIME);
#else
    InitSendTimer(SHORTTIME);
#endif
    SetSendTimerInterruptFunc(&SendTIMInterrup);
}

void DeInit_Sender() {
    DeinitSendTimer();
}

Frame send_frame;
unsigned int status = LISTEN;
unsigned int current_data = 0;
unsigned int bit_open = 0;

__inline void open(void) {
    //Log("open");
    led_orange_on();
    data_pin_setbit(Bit_RESET);
    bit_open = 1;
}

__inline void close(void) {
    //Log("close");
    led_orange_off();
    data_pin_setbit(Bit_SET);
    bit_open = 0;
}

__inline void return_int(void) {
    // TIM4->ARR -= 2;
}


void SendTIMInterrup(void) {
    static u8 bit;
    static u8 bit_pos;
    static u16 int_pos;

    SetSendTimerDelta(T_SIZE_DELAY);

    if(bit_open) {
        close();
        StopSendTimer();
        return;
    }

    if(status == SEND_DATA) {
        if(current_data >= (send_frame.frame.size + 2)*32) {
            current_data = 0;
            status = SEND_CRC;
        } else {
            int_pos = current_data / 32;
            bit_pos = (current_data - int_pos*32);

            //static char str[16];
            //_int2str(str, current_data, 10, 1, 0);
            //Log_nn("send cud: ");
            //Log_nn(str);

            //_int2str(str, bit_pos, 10, 1, 0);
            //Log_nn("      bit_pos: ");
            //Log_nn(str);
            //_int2str(str, int_pos, 10, 1, 0);
            //Log_nn("      int_pos: ");
            //Log_nn(str);

            //Log_nn("\n");

            open();
            bit = ((send_frame.data[int_pos] & (1 << bit_pos)) != 0);
            current_data++;

            SetSendTimerDelta(T_SIZE_ZERO + bit*(T_SIZE_ONE - T_SIZE_ZERO));
        }
    }

    if(status == SEND_CRC) {
        if(current_data == 32) {
            current_data = 0;
            bit_open = 0;
            status = LISTEN;
            StopSendTimer();
            DisableSendTimer();
            return;
        }

        //static char str[16];
        //_int2str(str, current_data, 10, 1, 0);
        //Log_nn("send cud: ");
        //Log_nn(str);
        //Log_nn("\n");

        open();
        bit = ((send_frame.frame.crc & (1 << current_data)) != 0);
        current_data++;
        //TIM4->CNT = 0;
        SetSendTimerDelta(T_SIZE_ZERO + bit*(T_SIZE_ONE - T_SIZE_ZERO));
    }

    StopSendTimer();
}

void SendFrame(Frame *frame) {
    if(status == LISTEN) {
        Log_nn("Send...  ");
        bit_open = 0;
        current_data = 0;
        send_frame = *frame;

        status = SEND_DATA;
        open();
        SetSendTimerDelta(T_SIZE_INIT);
        EnableSendTimer();
    }
}
