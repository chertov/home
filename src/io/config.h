#pragma once

#define DEBUG

#define     T_ERR           0.1   // процент отклонения от нормы
#define     T               64    // ~24 микросекунды
#define     T_SIZE_INIT     T*10  // period for init signal
#define     T_SIZE_ZERO     T*1   // period for bit 0 signal
#define     T_SIZE_ONE      T*2   // period for bit 1 signal   
#define     T_SIZE_DELAY    T     // period for delay signal
#define     T_SIZE_TIMEOUT  T*15  // period for timeout

#define     FRAME_MAX_DATA_SIZE     16 // bytes

//#define     USE_TIMEOUT_TIMER       true

//#define DATA_PIN_1_PORT   GPIOC
//#define DATA_PIN_1        GPIO_Pin_13
//
//#define DATA_PIN_2_PORT   GPIOC
//#define DATA_PIN_2        GPIO_Pin_0

#define     BUTTON      GPIO_Pin_0

#define     DWT_CYCCNT    *(volatile unsigned long *)0xE0001004
#define     DWT_CONTROL   *(volatile unsigned long *)0xE0001000
#define     SCB_DEMCR     *(volatile unsigned long *)0xE000EDFC

#define SHORTTIME 24-1
#define LONGTIME 720000-1
#ifdef DEBUG
    //#define ENABLE_LONGTIME
#endif
