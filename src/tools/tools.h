#pragma once
#include "hardware/platform.h"

#include "io/frame.h"
#include "stdbool.h"

void _int2str( char* ptr, s32 X, u16 digit, int flagunsigned, int fillwithzero );

uint32_t crc_calc(uint32_t * buffer, uint32_t buff_len);

uint32_t crc_frame(Frame *frame);
u8 test_crc_frame(Frame *frame);

void Delay(vu32 nCount);
void Delay_ms(vu32 milliseconds);

bool frameIs(const uint32_t t1, const uint32_t t2);