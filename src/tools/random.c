#include "random.h"

#include "hardware/adc.h"

uint32_t get_value(void)
{
    uint32_t res = 0;
    unsigned int current_char = 0;
    
    current_char = get_adc_value() & 0x01; res += current_char << 0;
    current_char = get_adc_value() & 0x01; res += current_char << 1;
    current_char = get_adc_value() & 0x01; res += current_char << 2;
    current_char = get_adc_value() & 0x01; res += current_char << 3;
    current_char = get_adc_value() & 0x01; res += current_char << 4;
    current_char = get_adc_value() & 0x01; res += current_char << 5;
    current_char = get_adc_value() & 0x01; res += current_char << 6;
    current_char = get_adc_value() & 0x01; res += current_char << 7;
    current_char = get_adc_value() & 0x01; res += current_char << 8;
    current_char = get_adc_value() & 0x01; res += current_char << 9;
    current_char = get_adc_value() & 0x01; res += current_char << 10;
    current_char = get_adc_value() & 0x01; res += current_char << 11;
    current_char = get_adc_value() & 0x01; res += current_char << 12;
    current_char = get_adc_value() & 0x01; res += current_char << 13;
    current_char = get_adc_value() & 0x01; res += current_char << 14;
    current_char = get_adc_value() & 0x01; res += current_char << 15;
    current_char = get_adc_value() & 0x01; res += current_char << 16;
    current_char = get_adc_value() & 0x01; res += current_char << 17;
    current_char = get_adc_value() & 0x01; res += current_char << 18;
    current_char = get_adc_value() & 0x01; res += current_char << 19;
    current_char = get_adc_value() & 0x01; res += current_char << 20;
    current_char = get_adc_value() & 0x01; res += current_char << 21;
    current_char = get_adc_value() & 0x01; res += current_char << 22;
    current_char = get_adc_value() & 0x01; res += current_char << 23;
    current_char = get_adc_value() & 0x01; res += current_char << 24;
    current_char = get_adc_value() & 0x01; res += current_char << 25;
    current_char = get_adc_value() & 0x01; res += current_char << 26;
    current_char = get_adc_value() & 0x01; res += current_char << 27;
    current_char = get_adc_value() & 0x01; res += current_char << 28;
    current_char = get_adc_value() & 0x01; res += current_char << 29;
    current_char = get_adc_value() & 0x01; res += current_char << 30;
    current_char = get_adc_value() & 0x01; res += current_char << 31;
    
    return res;
}

#define lfg_a 97
#define lfg_b 33
//#define lfg_a 17
//#define lfg_b 5


uint32_t lfgs[lfg_a];
unsigned char lfgs_ptr = 0;

void push(uint32_t val)
{
    lfgs[lfgs_ptr] = val;
    lfgs_ptr++;
    if(lfgs_ptr >= lfg_a) lfgs_ptr = 0;
}

uint32_t get(unsigned int back)
{
    static int pos = 0;
    pos = lfgs_ptr;
    back = back % lfg_a;
    pos -= back;
    if(pos < 0) pos += lfg_a;
    return lfgs[pos];
}

void Lfg_init(void)
{
    int i = 0;
    for(i = 1000*lfg_a; i > 0; i--)
        push(get_value());
}

uint32_t Lfg(void)
{
    static uint32_t x_a = 0;
    static uint32_t x_b = 0; 
    static uint32_t x = 0; 
    x_a = get(lfg_a);
    x_b = get(lfg_b);
    x = x_a - x_b;
    push(x);

    return x;
}

void Init_random(void)
{
    Init_adc();
    Lfg_init();
}
void DeInit_random(void)
{
    DeInit_adc();
}
uint32_t random(void)
{
    return Lfg();
}
