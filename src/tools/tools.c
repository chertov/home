#include "tools.h"

/**
*
*  Translate a 32 bit word into a string.
*
*  @param[in,out] ptr            A pointer to a string large enough to contain
*                                the translated 32 bit word.
*  @param[in]     X              The 32 bit word to translate.
*  @param[in]     digit          The amount of digits wanted in the result string.
*  @param[in]     flagunsigned   Is the input word unsigned?
*  @param[in]     fillwithzero   Fill with zeros or spaces.
*
**/
/******************************************************************************/
void _int2str( char* ptr, s32 X, u16 digit, int flagunsigned, int fillwithzero )
{
   u8    c;
   u8    fFirst   = 0;
   u8    fNeg     = 0;
   u32   DIG      = 1;
   int   i;
   u32 r;

   for( i = 1; i < digit; i++ )
      {
      DIG *= 10;
      }

   if( !flagunsigned && ( X < 0 ) )
      {
      fNeg = 1;
      X    = -X;
      }

   r = X;

   for( i = 0; i < digit; i++, DIG /= 10 )
      {
      c  = (r/DIG);
      r -= (c*DIG);

      if( fillwithzero || fFirst || c || ( i == ( digit - 1 ) ) )
         {
         if( ( fFirst == 0 ) && !flagunsigned )
            {
            *ptr++ = fNeg ? '-' : ' ';
            }

         *ptr++ = c + 0x30;
         fFirst = 1;
         }
       else
         {
         *ptr++ = ' ';
         }
      }

   *ptr++ = 0;
}


//Функция для аппаратного подсчёта CRC32
//Принимает на вход указатель на массив uint32_t чисел и размер этого массива
//Возвращает CRC32 для этого массива
uint32_t crc_calc(uint32_t *buffer, uint32_t buff_len)
{
    
    
    CRC_ResetDR ();
    return CRC_CalcBlockCRC(buffer, buff_len);
//  uint32_t i;
//  CRC->CR |= CRC_CR_RESET; //Делаем сброс...
//  for(i = 0; i < buff_len; i++) {
//      CRC->DR = buffer[i]; //Загоняем данные из буфера в регистр данных
//  }
//  return (CRC->DR); //Читаем контрольную сумму и возвращаем её
}

uint32_t crc_frame(Frame *frame)
{
    
    CRC_ResetDR (); //Делаем сброс...
    //Загоняем данные из буфера в регистр данных
    frame->frame.crc = CRC_CalcBlockCRC(frame->data, frame->frame.size+2);
    return (frame->frame.crc);  //Читаем контрольную сумму и возвращаем её
    
//  uint32_t i;
//  CRC->CR |= CRC_CR_RESET; //Делаем сброс...
//  for(i = 0; i < frame->frame.size+2; i++) {
//      CRC->DR = frame->data[i]; 
//  }
//  frame->frame.crc = CRC->DR;
//  return (frame->frame.crc); //Читаем контрольную сумму и возвращаем её
}

Frame frame_tmp;

u8 test_crc_frame(Frame *frame)
{
    uint32_t crc;
    
    // при копировании лучше запретить все прерывания
    frame_tmp = *frame;

//	uint32_t i;
//	CRC->CR |= CRC_CR_RESET; //Делаем сброс...
//	for(i = 0; i < frame_tmp.frame.size+2; i++) {
//		CRC->DR = frame_tmp.data[i]; //Загоняем данные из буфера в регистр данных
//	}
    
    CRC_ResetDR (); //Делаем сброс...
    //Загоняем данные из буфера в регистр данных
    crc = CRC_CalcBlockCRC(frame_tmp.data, frame_tmp.frame.size+2);
    
    return (frame_tmp.frame.crc == crc); //Читаем контрольную сумму и возвращаем её
}

void Delay(vu32 nCount) {
    for(; nCount!= 0;nCount--);
}


void Delay_ms(vu32 milliseconds) {
    volatile uint32_t nCount;
    RCC_ClocksTypeDef RCC_Clocks;
    RCC_GetClocksFreq(&RCC_Clocks);
    nCount = (RCC_Clocks.HCLK_Frequency/10000)*milliseconds;
    for(; nCount!= 0;nCount--);
}

bool frameIs(const uint32_t t1, const uint32_t t2) {
    return __fabs(1.0 - ((float)t1) / ((float)t2)) < T_ERR;
}