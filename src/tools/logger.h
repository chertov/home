#pragma once
#include "hardware/platform.h"

void Init_logger(void);
void DeInit_logger(void);

void Log_Data(void);

extern u16 main_count;
extern u16 main_time_summ;
extern u16 main_time_min;
extern u16 main_time_max;

extern u16 timer_count;
extern u16 timer_time_summ;
extern u16 timer_time_min;
extern u16 timer_time_max;
extern u16 timer_timeouts;

extern u8 logg;

extern u16 rcv_init;
extern u16 rcv_good;
extern u16 rcv_timeout;
extern u16 rcv_unknow;
