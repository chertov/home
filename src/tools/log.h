#pragma once
#include "hardware/platform.h"

void Init_log(void);
void DeInit_log(void);

void Log(const unsigned char *pucBuffer);
void Log_nn(const unsigned char *pucBuffer);

void Log_len(const unsigned char *pucBuffer, unsigned int len);
void Log_len_nn(const unsigned char *pucBuffer, unsigned int len);

void Log_uint(unsigned int value);
void Log_uint_nn(unsigned int value);
