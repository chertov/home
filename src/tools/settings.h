#pragma once
#include <stdbool.h>   

#include "hardware/platform.h"

//typedef struct
//{
//    u16 prio;
//    u16 src;
//    u16 dst;
//    u16 size;
//    u32 data[FRAME_MAX_DATA_SIZE];
//    u32 crc;
//} FrameData;

typedef struct
{
    char Type[128];
    char Name[128];
    u32 crc;
} Settings;

Settings default_settings(void);

bool get_settings(Settings *settings);
bool set_settings(Settings *settings);
