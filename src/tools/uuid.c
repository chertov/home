#include "uuid.h"

#include "random.h"

void Init_uuid(void)
{
}


uint32_t get_hard_uuid(void)
{
    CRC_ResetDR ();
    return CRC_CalcBlockCRC((uint32_t *)0x1FFFF7E8, 3);
}


#define FLASH_PAGE_SIZE    ((uint32_t)0x00000800)   /* FLASH Page Size */
#define FLASH_START_ADDR   ((uint32_t)0x08000000)   /* Start @ of user Flash area */

#define FLASH_USER_START_ADDR FLASH_START_ADDR+FLASH_PAGE_SIZE*100

uint32_t get_soft_uuid(void)
{
    uint32_t Address = FLASH_USER_START_ADDR;

    uint32_t uuid = 0;
    uint32_t uuid_crc = 0;
    uint32_t crc = 0;

    uuid = *(__IO uint32_t *)Address;
    Address += sizeof(uint32_t);
    uuid_crc = *(__IO uint32_t *)Address;
    
    CRC_ResetDR ();
    crc = CRC_CalcBlockCRC((uint32_t *)&uuid, 1);

    if(uuid_crc == crc)
        return uuid;
    else
        return 0;
}

void set_soft_uuid(uint32_t uuid_new, uint32_t uuid_crc)
{
    uint32_t Address;
    FLASH_Unlock(); //unlock flash writing
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR); 
    if (FLASH_ErasePage(FLASH_USER_START_ADDR)!= FLASH_COMPLETE)
    {
        // произошла ошибка стирания
    }
    Address = FLASH_USER_START_ADDR;
    if (FLASH_ProgramWord(Address, uuid_new) != FLASH_COMPLETE)
    {
        // произошла ошибка записи
    }
    Address += sizeof(uint32_t);
    if (FLASH_ProgramWord(Address, uuid_crc) != FLASH_COMPLETE)
    {
        // произошла ошибка записи
    }
    FLASH_Lock();//lock the flash for writing
}

uint32_t generate_uuid(void)
{
    uint32_t uuid_new = 0;
    uint32_t uuid_soft = 0;
    uint32_t uuid_crc = 0;

    uuid_new = random();
    CRC_ResetDR ();
    uuid_crc = CRC_CalcBlockCRC((uint32_t *)&uuid_new, 1);
    
    set_soft_uuid(uuid_new, uuid_crc);
    
    uuid_soft = get_soft_uuid();
    
    if(uuid_soft == uuid_new)
        return uuid_new;
    else
        return 0;
}


uint32_t get_uuid(void)
{
//    uint32_t uuid = get_hard_uuid();
//    Log_nn("My hard uuid: ");
//    Log_uint_nn(uuid);
//    Log_nn("\n");


    uint32_t soft_uuid = get_soft_uuid();
//    Log_nn("My soft uuid: ");
//    Log_uint_nn(soft_uuid);
//    Log_nn("\n");
    if(soft_uuid == 0)
    {
        soft_uuid = generate_uuid();
//        Log_nn("Generate new id: ");
//        Log_uint_nn(soft_uuid);
//        Log_nn("\n");

        soft_uuid = get_soft_uuid();
//        Log_nn("My soft uuid: ");
//        Log_uint_nn(soft_uuid);
//        Log_nn("\n");
    }
    return soft_uuid;
}
