#include "log.h"

#include "hardware/usart.h"


void Init_log()
{
    Init_usart();
}

void DeInit_log(void)
{
    DeInit_usart();
}

void Log(const unsigned char *pucBuffer)
{
    Log_nn(pucBuffer);
    Log_nn("\n");
}

#define NUM 100
void Log_nn(const unsigned char *pucBuffer)
{
    unsigned int k = 0;
    while(1)
    {
        if(pucBuffer[k] != '\0')
            send_to_uart(pucBuffer[k++]);
        else
        {
            //USBLogger_Send(pucBuffer, k);
            return;
        }
        if(k > NUM)
            return;
    }
}

void Log_len(const unsigned char *pucBuffer, unsigned int len)
{
    Log_len_nn(pucBuffer, len);
    Log_nn("\n");
}

void Log_len_nn(const unsigned char *pucBuffer, unsigned int len)
{
    unsigned int i = 0;
    for(i = 0; i < len; ++i)
        send_to_uart(pucBuffer[++i]);
}


char log_str[16];
void Log_uint(unsigned int value)
{
    _int2str(log_str, value, 10, 1, 0);
    Log_nn(log_str);
    Log_nn("\n");
}

void Log_uint_nn(unsigned int value)
{
    _int2str(log_str, value, 10, 1, 0);
    Log_nn(log_str);
}
