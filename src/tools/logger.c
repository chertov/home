#include "logger.h"

#include "io/config.h"
#include "tools.h"
#include "log.h"

#include <stm32f30x.h>
#include <stm32f30x_gpio.h>
#include <stm32f30x_rcc.h>
#include <stm32f30x_tim.h>
#include <stm32f30x_misc.h>

char logger_str[16];

u16 timer_count = 0;
u16 timer_time_summ = 0;
u16 timer_time_min = 99999999999;
u16 timer_time_max = 0;
u16 timer_timeouts = 0;
void Log_timer()
{
    Log_nn("timer   ");
    _int2str(logger_str, timer_time_summ / timer_count, 10, 1, 0);
    Log_nn(logger_str);
    Log_nn("   min: ");
    _int2str(logger_str, timer_time_min, 10, 1, 0);
    Log_nn(logger_str);
    Log_nn("   max: ");
    _int2str(logger_str, timer_time_max, 10, 1, 0);
    Log_nn(logger_str);

    Log_nn("   timeouts: ");
    _int2str(logger_str, timer_timeouts, 10, 1, 0);
    Log_nn(logger_str);

    Log_nn("\n");
}


u16 main_count = 0;
u16 main_time_summ = 0;
u16 main_time_min = 99999999999;
u16 main_time_max = 0;
void Log_main()
{
    Log_nn("main    ");
    _int2str(logger_str, main_time_summ / main_count, 10, 1, 0);
    Log_nn(logger_str);
    Log_nn("   min: ");
    _int2str(logger_str, main_time_min, 10, 1, 0);
    Log_nn(logger_str);
    Log_nn("   max: ");
    _int2str(logger_str, main_time_max, 10, 1, 0);
    Log_nn(logger_str);
    Log_nn("\n");
}


u16 rcv_init = 0;
u16 rcv_good = 0;
u16 rcv_timeout = 0;
u16 rcv_unknow = 0;
void Log_rcv()
{
    Log_nn("rcv     ");

    Log_nn("   init: ");
    _int2str(logger_str, rcv_init, 10, 1, 0);
    Log_nn(logger_str);

    Log_nn("   good: ");
    _int2str(logger_str, rcv_good, 10, 1, 0);
    Log_nn(logger_str);

    Log_nn("   timeout: ");
    _int2str(logger_str, rcv_timeout, 10, 1, 0);
    Log_nn(logger_str);

    Log_nn("   unknow: ");
    _int2str(logger_str, rcv_unknow, 10, 1, 0);
    Log_nn(logger_str);

    Log_nn("\n");
}

void Log_Data(void)
{
    //Log_main();
    //Log_timer();
    Log_rcv();
}

void Init_logger(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    //RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

//    //Включаем таймер 6
//    LOGGER_TIMER->PSC = 24000 - 1;            // Настраиваем делитель что таймер тикал 1000 раз в секунду
//    LOGGER_TIMER->ARR = LOGGER_TIME * 1000;   // Чтоб прерывание случалось раз в секунду
//    LOGGER_TIMER->DIER |= TIM_DIER_UIE;       // Разрешаем прерывание от таймера
//    LOGGER_TIMER->CR1 |= TIM_CR1_CEN;         // Начать отсчёт!

//    // NVIC_EnableIRQ(TIM6_DAC_IRQn);         // Разрешение TIM6_DAC_IRQn прерывания


//    /* Инициализируем базовый таймер: делитель 24000, период 500 мс.
//    * Другие параметры структуры TIM_TimeBaseInitTypeDef
//    * не имеют смысла для базовых таймеров.
//    */
//    TIM_TimeBaseInitTypeDef base_timer;
//    TIM_TimeBaseStructInit(&base_timer);
//    /* Делитель учитывается как TIM_Prescaler + 1, поэтому отнимаем 1 */
//    base_timer.TIM_Prescaler = 24000 - 1;
//    base_timer.TIM_Period = 500;
//    TIM_TimeBaseInit(LOGGER_TIMER, &base_timer);

//    /* Разрешаем прерывание по обновлению (в данном случае -
//    * по переполнению) счётчика таймера TIM6.
//    */
//    TIM_ITConfig(LOGGER_TIMER, TIM_IT_Update, ENABLE);
//    /* Включаем таймер */
//    TIM_Cmd(LOGGER_TIMER, ENABLE);

//    /* Разрешаем обработку прерывания по переполнению счётчика
//     * таймера TIM6. Так получилось, что это же прерывание
//     * отвечает и за опустошение ЦАП.
//     */
//    //NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);     // Разрешение TIM6_DAC_IRQn прерывания
//    // NVIC_EnableIRQ(TIM1_UP_IRQn);          // Разрешение TIM6_DAC_IRQn прерывания


    TIM_TimeBaseStructure.TIM_Prescaler = 7200;     // Настраиваем делитель что таймер тикал 1000 раз в секунду
    TIM_TimeBaseStructure.TIM_Period = 20000;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit( TIM4, &TIM_TimeBaseStructure );

    /* Enable the TIM3 gloabal Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    
    TIM_ClearITPendingBit( TIM4, TIM_IT_Update );
    TIM_ITConfig( TIM4, TIM_IT_Update, ENABLE);    
    TIM_Cmd( TIM4, ENABLE );
}

u32 logg_count = 0;
u8 logg = 1;

void DeInit_logger(void)
{
    TIM_DeInit(TIM4);
}
// void TIM1_UP_IRQHandler(void)    // Для STM32VLDISCOVERY    STM32F100RBT6B

//void SysTick_Handler(void)
//void TIM1_UP_TIM16_IRQHandler(void)
void TIM4_IRQHandler()
{
    TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
    Log_nn("log  ");
    _int2str(logger_str, logg_count, 10, 1, 0);
    Log_nn(logger_str);
    Log_nn("\n");
    logg_count++;
    logg = 1;

//  /* Включаем таймер */
//  TIM_Cmd(TIM2, DISABLE);
//  TIM2->SR &= ~TIM_SR_UIF;    // Сбрасываем флаг UIF
}
