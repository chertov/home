#pragma once
#include "hardware/platform.h"

void Init_uuid(void);
uint32_t get_soft_uuid(void);
uint32_t get_hard_uuid(void);
uint32_t generate_uuid(void);
uint32_t get_uuid(void);
