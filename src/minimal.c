#include "hardware/leds.h"
#include <stm32f30x_conf.h>
#include <stm32f30x_misc.h>
#include <stm32f30x.h>

#include "tools/log.h"
#include "tools/logger.h"

#include "bootloader.h"

void Init_rcc()
{
    ErrorStatus HSEStartUpStatus;

    RCC_DeInit();
    RCC_HSEConfig(RCC_HSE_ON);
    HSEStartUpStatus = RCC_WaitForHSEStartUp();
    
    if(HSEStartUpStatus == SUCCESS)
    {
        // HCLK = SYSCLK
        RCC_HCLKConfig(RCC_SYSCLK_Div1);
        // PCLK2 = HCLK
        RCC_PCLK2Config(RCC_HCLK_Div1);
        // PCLK1 = HCLK/2
        RCC_PCLK1Config(RCC_HCLK_Div2);

      // PLLCLK = 8 MHz * 9 = 72 MHz
        RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_9);
        
        // Разрешение PLL
        RCC_PLLCmd(ENABLE);
    
        // Ожидание готовности PLL
        while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET) {};
        
        // выбор PLL как источника системного тактирования
        RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
        
        // Ожидание готовности PLL для использования как источника системного тактирования
        while(RCC_GetSYSCLKSource() != 0x08) {}
    }
}

int main(void)
{
    SystemInit();
    __disable_irq();
    // инициализация таблицы векторов прерываний после прыжка
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, AppilcationAddress);
    __enable_irq();

//  Init_rcc();
    Init_log();


    Init_leds();
    while(1)
    {
        led_green_on();
        Delay(5000000);
        led_green_off();
        Delay(15000000);
        Log("Hello from app1");
    }
}
