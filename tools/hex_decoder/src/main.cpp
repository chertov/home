
#include <iostream>
#include <fstream>
#include <vector>

#include "Console.h"

namespace con = JadedHoboConsole;



enum RecordType
{
    DATA_RECORD = 0x00,
    EOF_RECORD	= 0x01,
    EXT_SEGMENT_ADR = 0x02,
    EXT_LINEAR_ADR	= 0x04,
    START_LINEAR_ADR = 0x05
};

// : - hex format
// ll - record length
// aaaa - is the address field that represents starting address for subsequent data in the record
// tt - hex record type
//     00 - data record
//     01 - end-of-file record
//     02 - extended segment address record
//     04 - extended linear address record
//     05 - start linear address record (MDK-ARM only)
// dd - data
// cc - checksum

class AddressByte
{
public:
    unsigned int address;
    unsigned char byte;
};

class Record
{
public:

    Record()
    {

    }
    
    Record(std::string line)
    {
        this->line = line;
        if (!parse())
        {
            std::cout << con::fg_red << "bad crc" << con::fg_white << std::endl;
            system("pause");
            exit(EXIT_FAILURE);
        }
    }

    std::string line;

    RecordType type;
    unsigned int address;
    std::vector<unsigned char> data;
    unsigned int checksum;

    unsigned int get_crc()
    {
        unsigned char summ = 0;
        for (unsigned int i = 0; i < ((line.size() - 1) / 2 - 1); i++)
        {
            std::string hex = line.substr(1 + i * 2, 2);
            summ += std::stoul(hex, nullptr, 16);
        }
        summ %= 256;
        return 256 - summ;
    }

    bool parse()
    {
        unsigned int line_len = line.size();

        unsigned int pos = 0;
        unsigned int size = 0;

        size = 1;  std::string hex_format = line.substr(pos, size);  pos += size;
        size = 2;  std::string ll_str = line.substr(pos, size);  pos += size;

        unsigned int ll = std::stoul(ll_str, nullptr, 16);

        size = 4;  std::string aaaa_str = line.substr(pos, size);  pos += size;
        size = 2;  std::string record_type_str = line.substr(pos, size);  pos += size;
        size = ll * 2;  std::string data_str = line.substr(pos, size);  pos += size;
        size = 2;  std::string crc_str = line.substr(pos, size);  pos += size;

        type = static_cast<RecordType>(std::stoul(record_type_str, nullptr, 16));
        address = std::stoul(aaaa_str, nullptr, 16);

        for (unsigned int i = 0; i < ll; i++)
            data.push_back(std::stoul(data_str.substr(i * 2, 2), nullptr, 16));

        unsigned int original_crc = std::stoul(crc_str, nullptr, 16);
        unsigned char crc = get_crc();

        if (crc == original_crc)
            return true;
        return false;
    }

};


class Firmware
{
public:
    unsigned int start_address;
    unsigned int ext_linear_address;
    std::vector<unsigned char> data;
    std::vector<unsigned int> data_int;

    bool generate()
    {
        if (data.size() % 4 != 0)
        {
            std::cout << con::fg_red << "not a bytes" << con::fg_white << std::endl;
            return false;
        }

        unsigned int byte4;
        unsigned char* byte4_ptr = reinterpret_cast<unsigned char*>(&byte4);
        for (unsigned int i = 0; i < data.size() / 4; ++i)
        {
            byte4_ptr[0] = data[i * 4 + 0];
            byte4_ptr[1] = data[i * 4 + 1];
            byte4_ptr[2] = data[i * 4 + 2];
            byte4_ptr[3] = data[i * 4 + 3];

            data_int.push_back(byte4);
        }
        return true;
    }
};

void compare(std::string path_1, std::string path_2)
{
    std::fstream file_1, file_2;
    file_1.open(path_1.c_str());
    file_2.open(path_2.c_str());

    if (file_1.is_open() && file_2.is_open())
    {
        unsigned int line_counter = 0;

        std::string line_1, line_2;
        while (std::getline(file_1, line_1) && std::getline(file_2, line_2))
        {
            line_1 = line_1.substr(7);
            line_2 = line_2.substr(7);


            if (line_1.size() != line_2.size())
            {
                system("pause");
                exit(EXIT_FAILURE);
            }

            unsigned int len = line_1.size();

            line_1 = line_1.substr(0, len - 2);
            line_2 = line_2.substr(0, len - 2);
            len = line_1.size();

            if (line_1 == line_2)
            {
                line_counter++;
                continue;
            }


            std::cout << con::fg_yellow << line_counter << "   " << con::fg_white;
            for (unsigned int i = 0; i < len; i++)
            {
                if (i == 2) std::cout << "   ";
                if (i == 10 || i == 18 || i == 26) std::cout << " ";
                if (line_1[i] == line_2[i])
                    std::cout << con::fg_white;
                else
                    std::cout << con::fg_green;
                std::cout << line_1[i] << con::fg_white;
            }
            std::cout << std::endl;

            std::cout << con::fg_yellow << line_counter << "   " << con::fg_white;
            for (unsigned int i = 0; i < len; i++)
            {
                if (i == 2) std::cout << "   ";
                if (i == 10 || i == 18 || i == 26) std::cout << " ";
                if (line_1[i] == line_2[i])
                    std::cout << con::fg_white;
                else
                    std::cout << con::fg_green;
                std::cout << line_2[i] << con::fg_white;
            }
            std::cout << std::endl;
            //std::cout << line_1 << std::endl;
            //std::cout << line_2 << std::endl;

            std::cout << std::endl;
            line_counter++;
            //system("pause");
        }
    }

    system("pause");
    exit(EXIT_FAILURE);
}

bool load_firmware(std::string path, Firmware &firmware)
{
    std::fstream file;
    file.open(path.c_str());

    if (file.is_open())
    {
        std::vector<Record> data_records;
        std::vector<Record> header_records;

        Record EXT_LINEAR_ADR_record;

        std::string line;
        while (std::getline(file, line))
        {
            //std::cout << line << std::endl;
            Record record(line);

            if (record.type == DATA_RECORD)
                data_records.push_back(record);
            else if (record.type == EXT_LINEAR_ADR)
                EXT_LINEAR_ADR_record = record;
            else if (record.type == EOF_RECORD)
                break;
        }

        std::vector<AddressByte> bytes;

        for (unsigned int i = 0; i < data_records.size(); i++)
        {
            Record record = data_records[i];
            for (unsigned int d = 0; d < record.data.size(); d++)
            {
                AddressByte byte;
                byte.address = record.address + d;
                byte.byte = record.data[d];

                bytes.push_back(byte);
            }
        }

        unsigned int address = bytes[0].address;
        firmware.ext_linear_address = (EXT_LINEAR_ADR_record.data[0] * 256 + EXT_LINEAR_ADR_record.data[1]) * 256 * 256;
        firmware.start_address = firmware.ext_linear_address + address;
        std::cout << std::hex << firmware.start_address << std::endl;

        firmware.data.clear();
        for (unsigned int i = 0; i < bytes.size(); i++)
        {
            if (bytes[i].address != address)
            {
                std::cout << con::fg_red << "bad offset" << con::fg_white << std::endl;
                system("pause");
                exit(EXIT_FAILURE);
            }
            firmware.data.push_back(bytes[i].byte);
            ++address;
        }

        firmware.generate();

        file.close();
        return true;
        //std::cout << std::hex << firmware.start_address << std::endl;
        //std::ofstream out("out.txt", std::ios_base::trunc | std::ios_base::binary);

        //for (unsigned int i = 0; i < firmware.data.size(); i++)
        //    out << firmware.data[i];
        //out.close();
    }

    file.close();
    std::cout << "Can't open file" << path << std::endl;
    return false;
}

int main()
{
    // delta 113450

    // delta data 345
    //compare("home_8123450.hex", "home_0000000.hex");

    Firmware home_8123450;
    Firmware home_0000000;

    //compare("home_8123450.hex", "home_0000000.hex");
    if(!load_firmware("home_8123450.hex", home_8123450))
    {
        std::cout << "Can't load 8123450" << std::endl;
        system("pause");
    }
    if(!load_firmware("home_8010000.hex", home_0000000))
    {
        std::cout << "Can't load 0000000" << std::endl;
        system("pause");
    }

    for (unsigned int i = 0; i < home_0000000.data_int.size(); ++i)
    {
        if (home_0000000.data_int[i] != home_8123450.data_int[i])
        {
            std::cout << std::hex << home_0000000.data_int[i] << "     " << home_8123450.data_int[i];

            unsigned int delta = home_8123450.data_int[i] - home_0000000.data_int[i];
            std::cout << "       " << std::hex << delta << std::endl;
        }
    }

    //compare("home_8000000.hex", "home_8010000.hex");
    //compare("home_0000000.hex", "home_8020100.hex");
    //std::fstream file;
    //file.open("bootloader.hex");
    //file.open("bootloader.hex");
    //file.open("home_8000000.hex");
    //file.open("home_8010000.hex");

    //if (file.is_open())
 //   {
 //       std::vector<Record> data_records;
 //       std::vector<Record> header_records;

 //       Record EXT_LINEAR_ADR_record;

    //	std::string line;
    //	while (std::getline(file, line))
    //	{
 //           std::cout << line << std::endl;
 //           Record record(line);

 //           if (record.type == DATA_RECORD)
 //               data_records.push_back(record);
 //           else if (record.type == EXT_LINEAR_ADR)
 //               EXT_LINEAR_ADR_record = record;
 //           else if (record.type == EOF_RECORD)
 //               break;
    //	}

 //       std::vector<AddressByte> bytes;

 //       for (unsigned int i = 0; i < data_records.size(); i++)
 //       {
 //           Record record = data_records[i];
 //           for (unsigned int d = 0; d < record.data.size(); d++)
 //           {
 //               AddressByte byte;
 //               byte.address = record.address + d;
 //               byte.byte = record.data[d];

 //               bytes.push_back(byte);
 //           }
 //       }

 //       Firmware firmware;
 //       firmware.start_address = (EXT_LINEAR_ADR_record.data[0] * 256 + EXT_LINEAR_ADR_record.data[1]) * 256 * 256;

 //       unsigned int address = 0;
 //       for (unsigned int i = 0; i < bytes.size(); i++)
 //       {
 //           if (bytes[i].address != address)
 //           {
 //               std::cout << con::fg_red << "bad offset" << con::fg_white << std::endl;
 //               system("pause");
 //               exit(EXIT_FAILURE);
 //           }
 //           firmware.data.push_back(bytes[i].byte);
 //           ++address;
 //       }

 //       std::cout << std::hex << firmware.start_address << std::endl;
 //       std::ofstream out("out.txt", std::ios_base::trunc | std::ios_base::binary);

 //       for (unsigned int i = 0; i < firmware.data.size(); i++)
 //           out << firmware.data[i];
 //       out.close();
 //       
    //	system("pause");
    //}
    //else
    //{
    //	std::cout << "Can't open file" << std::endl;
    //	system("pause");
    //}

    return EXIT_SUCCESS;
}
